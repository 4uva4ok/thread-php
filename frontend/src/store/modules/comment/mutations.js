import { commentMapper } from '@/services/Normalizer';
import { SET_COMMENTS, ADD_COMMENT, DELETE_COMMENT } from './mutationTypes';

export default {
    [SET_COMMENTS]: (state, comments) => {
        state.comments = {
            ...state.comments,
            ...comments.reduce(
                (prev, comment) => ({ ...prev, [comment.id]: commentMapper(comment) }),
                {}
            ),
        };
    },

    [ADD_COMMENT]: (state, comment) => {
        state.comments = {
            ...state.comments,
            [comment.id]: commentMapper(comment)
        };
    },


    [DELETE_COMMENT]: (state, id) => {
        delete state.comments[id];

        state.comments = {
            ...state.comments,
        };
    },
};
